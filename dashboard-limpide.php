<?php
/*
* Plugin Name: Dashboard Limpide
* Plugin URI: https://limpide.fr/
* Description: À partir du tableau de bord Wordpress, accédez à votre espace connecté, suivez l'amélioration de votre site par Limpide et bien plus.
* Version: 1.51
* Author: Limpide
* Author URI: https://limpide.fr/a-propos/
*/

/*
préfixe : wpdali__
*/

/*
Chronologie
1/ Activation conditionnelle du plugin avec $wpdali__request
2/ Récupération du contenu en ajax avec wpdali__ajax_fill_plugin_content()
3/ Tracking GA t=pageview grace à jQuery(document).ready => $.get(url-specifique) présent dans le contenu qui vient d'être récupéré
*/

$wpdali__url_for_dashboard = [
    'domain' => 'https://limpide.fr/',
    'page' => 'generation-du-dashboard-limpide',
    'origin' => esc_url( home_url( '/' ) )
];

if ( !function_exists( 'wpdali__inner_content' ) ) {
    function wpdali__inner_content()
    {
        echo '
        <div id="wpdali__widget_limpide">
            <div id="wpdali__ajax_loader"
                style="display:flex;
                justify-content:center;
                align-items: center;
                width:100%;
                padding:20px;">
                <img src="' . plugin_dir_url( __FILE__ ) . 'img/ajax-loader.gif">
            </div>
        </div>
        ';
    }
}

if ( !function_exists( 'wpdali__widget' ) ) {
    function wpdali__widget()
    {
        global $wp_meta_boxes;
        $wp_meta_boxes['dashboard']['column3'] = $wp_meta_boxes['dashboard']['normal'];
        unset( $wp_meta_boxes['dashboard']['normal'] );

        wp_add_dashboard_widget( 'wpdali__widget', 'Dashboard Limpide', 'wpdali__inner_content', null, null, 'normal', 'high' );
    }
}

if ( !function_exists( 'wpdali__enqueue_admin' ) ) {
    function wpdali__enqueue_admin()
    {
        wp_enqueue_style( 'limpide-admin', plugin_dir_url( __FILE__ ) . 'dashboard-limpide.css' );
    }
}

/* AJAX */
if ( !function_exists( 'wpdali__ajax_fill_plugin_content' ) ) {
    function wpdali__ajax_fill_plugin_content()
    {
        if ( 'dashboard' !== get_current_screen()->id ) {
            return;
        }

        global $wpdali__url_for_dashboard;
        $url = $wpdali__url_for_dashboard['domain'] . $wpdali__url_for_dashboard['page'] . '/?origin=' . $wpdali__url_for_dashboard['origin'];
        // $data = 'NO DATA';
        // $dataType = 'jQuery guess'
        ?>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                // Récupération du contenu
                $.post('<?php echo $url; ?>', function(data) {
                    console.log('%cDashbord Limpide' + '%c mis à jour', 'background: #222; color: Gold', 'color: #07e3e2');
                    // console.log('%c Origine : ' + '%c<?php // echo $wpdali__url_for_dashboard['domain']; ?>', 'color: #07e3e2', 'color: #07e3e2');
                    $('#wpdali__widget_limpide').fadeOut('slow', function() {
                        $('#wpdali__ajax_loader').css({'display': 'none'});
                        $('#wpdali__widget_limpide').append(data);
                        $('#wpdali__widget_limpide').fadeIn('slow');               
                    });
                }).fail(function(data) {
                    $('#wpdali__widget').remove();
                });
            });
        </script>
        <?php
    }
}

/*
Activation conditionnelle du plugin
Si wp_remote_get renvoie true
=> l'option "Activer le mode test" n'a pas été cochée dans le BO de limpide.fr
=> on "active" le plugin
*/
$wpdali__request = wp_remote_get( $wpdali__url_for_dashboard['domain'] . $wpdali__url_for_dashboard['page'] . '/?origin=' . $wpdali__url_for_dashboard['origin'] );
$wpdali__raw_response = wp_remote_retrieve_body( $wpdali__request );
$wpdali__json = json_decode( $wpdali__raw_response ) ?: false;
if ( $wpdali__json ) {
    $wpdali__json_sans_mode_test = $wpdali__json ? $wpdali__json->sans_mode_test : true;
    if ( $wpdali__json_sans_mode_test ) {
        add_action( 'wp_dashboard_setup', 'wpdali__widget', 99.99 );
        add_action( 'admin_enqueue_scripts', 'wpdali__enqueue_admin' );
        add_action( 'admin_footer', 'wpdali__ajax_fill_plugin_content' );
    }
}
